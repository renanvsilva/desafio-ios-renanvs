//
//  CONPullRequestsController.swift
//  Concrete_challenge
//
//  Created by renan silva on 9/18/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit

class CONPullRequestsController: CONBaseViewController {

    var listPull = [CONPullModel]()
    var repoModel : CONRepoModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = repoModel.repoName
        CONRequestService.requestPullRequests(repoModel: repoModel, pullResult: { (list : [CONPullModel]) in
            self.listPull = list
            self.tableView.reloadData()
        }) { (e : Error?) in
                print("sdfdsf")
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CONPullCell") as! CONPullCell
        let model = listPull[indexPath.row]
        cell.setupPull(model: model)
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPull.count
    }
    
}
