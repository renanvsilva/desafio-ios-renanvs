//
//  CONBaseViewController.swift
//
//  Created by renan silva on 9/15/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit

class CONBaseViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        configNavigationBar()
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    var hideNavigationBar : Bool = false{
        didSet{
            self.navigationController?.setNavigationBarHidden(hideNavigationBar, animated: false)
        }
    }

    func configNavigationBar(){
        if self.navigationController == nil{
            return
        }
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }

}
