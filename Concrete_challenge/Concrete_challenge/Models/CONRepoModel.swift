//
//  CONRepoModel.swift
//  Concrete_challenge
//
//  Created by renan silva on 9/18/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit

class CONRepoModel: NSObject{
    
    var repoName : String!
    var descriptionText : String!
    var qtyFork : NSNumber!
    var qtyStar : NSNumber!
    var identifier : NSNumber!
    var ownerImageURI : String?
    var ownerName : String!
    
    class func parseDictionaryToModel(rootdic : NSDictionary) -> [CONRepoModel]{
        var modelList = [CONRepoModel]()
        let items = rootdic["items"] as? [NSDictionary]
        if let items = items{
            
            for item in items{
                let model = CONRepoModel()
                model.repoName = item.safeString(forKey: "name")
                model.descriptionText = item.safeString(forKey: "description")
                model.qtyFork = item.safeNumber(forKey: "forks_count")
                model.qtyStar = item.safeNumber(forKey: "stargazers_count")
                model.identifier = item.safeNumber(forKey: "id")
                let ownerDic = item["owner"] as! NSDictionary
                model.ownerName = ownerDic.safeString(forKey: "login")
                model.ownerImageURI = ownerDic.safeString(forKey: "avatar_url")
                modelList.append(model)
            }
        }

        return modelList
    }

}

