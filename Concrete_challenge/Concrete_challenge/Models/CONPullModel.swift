//
//  CONOwnerModel.swift
//  Concrete_challenge
//
//  Created by renan silva on 9/18/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit

class CONPullModel: NSObject{
    var title : String!
    var body : String!
    var userName : String!
    var dateFriendly : String!
    var identifier : NSNumber!
    var userImageURI : String?
    var date : String!{
        didSet{
            let formatter1 = DateFormatter()
            formatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            let dateObj = formatter1.date(from: date)
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "dd/MM/yyyy"
            dateFriendly = formatter2.string(from: dateObj!)
        }
    }
    
    
    class func parseArrayToModel(rootList : NSArray) -> [CONPullModel]{
        var modelList = [CONPullModel]()
        for item in rootList as! [NSDictionary]{
            let model = CONPullModel()
            model.title = item.safeString(forKey: "title")
            model.body = item.safeString(forKey: "body")
            model.identifier = item.safeNumber(forKey: "id")
            let ownerDic = item["user"] as! NSDictionary
            model.userName = ownerDic.safeString(forKey: "login")
            model.userImageURI = ownerDic.safeString(forKey: "avatar_url")
            
            model.date = item.safeString(forKey: "created_at")
            modelList.append(model)
        }
        
        return modelList
    }

}

