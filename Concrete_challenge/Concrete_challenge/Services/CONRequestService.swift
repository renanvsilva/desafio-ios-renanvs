//
//  CONRequestService.swift
//  CONnTest
//
//  Created by renan silva on 9/16/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit

class CONRequestService: CONBaseAPIService {
    
    static let repoBaseURL = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%@"
    static let pullsBaseURL = "https://api.github.com/repos/%@/%@/pulls"
    
    class func requestRepos(page : Int?, repoResult : @escaping ([CONRepoModel])->(), error : ((Error?)->())?){
        
        var _page : String = "1"
        if let _ = page{
            _page = "\(page!)"
        }
        
        let uri = String(format: repoBaseURL, _page)
        CONRequestService.requestGET(url: uri, params: nil, isJsonResponse: true, success: { (obj) in
            let listRepo = CONRepoModel.parseDictionaryToModel(rootdic: (obj! as! NSDictionary))
            repoResult(listRepo)
        }) { (e : Error?, r : URLResponse?) in
            print("e")
        }
    }

    class func requestPullRequests(repoModel : CONRepoModel, pullResult : @escaping ([CONPullModel])->(), error : ((Error?)->())?){
        
        let uri = String(format: pullsBaseURL, repoModel.ownerName, repoModel.repoName)
        CONRequestService.requestGET(url: uri, params: nil, isJsonResponse: true, success: { (obj) in
            let listPull = CONPullModel.parseArrayToModel(rootList: obj! as! NSArray)
            pullResult(listPull)
        }) { (e : Error?, r : URLResponse?) in
            print("e")
        }
    }

}
