//
//  CONBaseAPIService.swift
//  CONnTest
//
//  Created by renan silva on 9/16/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit
import AFNetworking

class CONBaseAPIService: NSObject {

    class func requestGET(url : String, params : [String : Any]?, isJsonResponse: Bool, success : ((Any?)->())?, error : ((Error?, URLResponse?)->())?){
        
        let sessionManager = AFHTTPSessionManager()
        sessionManager.responseSerializer = AFHTTPResponseSerializer()
        
        if isJsonResponse == true{
            sessionManager.responseSerializer = AFJSONResponseSerializer()
        }
        
        sessionManager.get(url, parameters: params, progress: nil, success: { (session : URLSessionDataTask, obj : Any?) in
            
            if (success != nil){
                success!(obj as Any?)
            }
            
        }) { (session : URLSessionDataTask?, e : Error) in
            if (error != nil){
                error!(e, session?.response)
            }
        }
        
    }
    
    class func requestPOST(url : String, params : [String : Any]?, success : ((Any?)->())?, error : ((Error?, URLResponse?)->())?){
        
        let sessionManager = AFHTTPSessionManager()
        sessionManager.responseSerializer = AFHTTPResponseSerializer()
        
        sessionManager.post(url, parameters: params, progress: nil, success: { (session : URLSessionDataTask, obj :Any?) in
            
            if (success != nil){
                success!(obj)
            }
            
        }) { (session : URLSessionDataTask?, e : Error) in
            if (error != nil){
                error!(e, session?.response)
            }
        }
    }
    
}
