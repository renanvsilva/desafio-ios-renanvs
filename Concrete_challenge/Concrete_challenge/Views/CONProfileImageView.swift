//
//  CONProfileImageView.swift
//  Concrete_challenge
//
//  Created by renan silva on 9/18/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit
import Kingfisher

class CONProfileImageView: UIImageView {

    var modelRepo : CONRepoModel!{
        didSet{
            if String.isEmptyStr(str: modelRepo.ownerImageURI){
                self.image = UIImage(named: "avatar.png")
                return
            }
            let url = URL(string: modelRepo.ownerImageURI!)
            self.kf.setImage(with: url)
        }
    }
    
    var modelPull : CONPullModel!{
        didSet{
            if modelPull.userImageURI == nil{
                self.image = UIImage(named: "avatar.png")
            }
            let url = URL(string: modelPull.userImageURI!)
            self.kf.setImage(with: url)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        config()
    }
    
    func config(){
        self.cornerRadius(value: self.widthSize()/2)
    }

}
