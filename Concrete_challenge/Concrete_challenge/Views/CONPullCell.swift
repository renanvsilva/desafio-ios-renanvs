//
//  CONPullCell.swift
//  Concrete_challenge
//
//  Created by renan silva on 9/18/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit

class CONPullCell: UITableViewCell {

    @IBOutlet weak var userImage : CONProfileImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var bodyLabel : UILabel!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    
    var repoModel : CONRepoModel!
    
    func setupPull(model : CONPullModel){
        userImage.modelPull = model
        titleLabel.text = model.title
        bodyLabel.text = model.body
        userNameLabel.text = model.userName
        dateLabel.text = model.dateFriendly
    }

}
