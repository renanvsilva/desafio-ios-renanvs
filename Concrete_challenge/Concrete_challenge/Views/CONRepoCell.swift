//
//
//  Created by renan silva on 9/15/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit

class CONRepoCell: UITableViewCell {

    @IBOutlet weak var ownerImage : CONProfileImageView!
    @IBOutlet weak var repoNameLabel : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!
    @IBOutlet weak var ownerNameLabel : UILabel!
    @IBOutlet weak var qtyForkLabel : UILabel!
    @IBOutlet weak var qtyStarLabel : UILabel!
    
    var repoModel : CONRepoModel!
    
    func setupRepo(model : CONRepoModel){
        ownerImage.modelRepo = model
        repoNameLabel.text = model.repoName
        descriptionLabel.text = model.descriptionText
        ownerNameLabel.text = model.ownerName
        
        qtyForkLabel.text = getNumberFormated(value: model.qtyFork.intValue)
        qtyStarLabel.text = getNumberFormated(value: model.qtyStar.intValue)
        
    }
    
    func getNumberFormated(value : Int) -> String{
        var text = ""
        if value < 1000{
            text = "\(value)"
        }else if value >= 1000 && value < 1000000{
            text = "\(value/1000)k"
        }else{
            text = "\(value/1000000)M"
        }
        return text
    }
}
