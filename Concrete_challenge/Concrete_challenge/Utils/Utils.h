//
//  Utils.h
//  PaleoProject
//
//  Created by renan veloso silva on 22/02/14.
//  Copyright (c) 2014 renan veloso silva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Utils;

@interface Utils : NSObject

+(CGRect) screenBoundsOnCurrentOrientation;

+(CGRect)keyboardRectWithNotification:(NSNotification*)notification;

+(id)loadNibForName:(NSString*)nibName;

@end

@interface NSString (custom)

+ (BOOL)isStringEmpty:(NSString *)string;

@end

@interface UIView (Additions)

-(void)centerHorizontal;

-(void)centerVertical;

-(void)centerHorizontalWithSuperView:(UIView*)sv;

-(void)centerWithSuperView:(UIView*)sv;

-(void)centerInSuperview;

-(void) setX:(float) newX;
-(void) setX_CG:(CGFloat) newX;
-(float)x;
-(CGFloat)x_CG;

-(void) setY:(float) newY;
-(void) setY_CG:(CGFloat) newY;
-(CGFloat)y_CG;

-(void) setWidth:(CGFloat) newWidth;
-(CGFloat)widthSize;

-(void) setHeight:(CGFloat) newHeight;
-(CGFloat)height;

-(void)removeSubviews;

-(void)clearColor;

@end

@interface UIColor (Aditions)

+ (UIColor *) colorWithHexString: (NSString *) hexString;

@end

@interface NSDictionary (Aditions)

-(NSString*)safeStringForKey:(NSString*)string;
-(NSNumber*)safeNumberForKey:(NSString*)string;

@end
