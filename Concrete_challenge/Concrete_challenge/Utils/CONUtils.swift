//
//
//  Created by renan silva on 9/15/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit

class CONUtils: NSObject {
    
    class func warninglAlert(message : String){
        let alert = UIAlertController(title: "Atenção", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        UIApplication.shared.keyWindow!.rootViewController?.present(alert, animated: true, completion: nil)
    }
}
