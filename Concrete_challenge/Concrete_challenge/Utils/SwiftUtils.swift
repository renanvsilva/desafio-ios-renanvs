//
//  SwiftUtils.swift
//  Soho
//
//  Created by renanvs on 11/3/15.
//  Copyright © 2015 lookr. All rights reserved.
//

import UIKit
import Foundation

extension String {
    static func isEmptyStr(str : String?)->Bool{
        if let s = str{
            if let ss = s as String?{
                if ss.isEmpty == false{
                    return false
                }
            }
        }
        
        return true
    }
    
    func integerValue() -> Int{
        return (self as NSString).integerValue
    }
    
    func intValue() -> Int32{
        return (self as NSString).intValue
    }
    
    func floatValue() -> Float{
        return (self as NSString).floatValue
    }
}


