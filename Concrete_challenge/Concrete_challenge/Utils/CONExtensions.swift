//
//
//  Created by renan silva on 9/17/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit

class CONExtensions: NSObject {}

extension UIView{
    func cornerRadius(value : CGFloat){
        self.layer.cornerRadius = value
        self.layer.masksToBounds = true
    }
}
