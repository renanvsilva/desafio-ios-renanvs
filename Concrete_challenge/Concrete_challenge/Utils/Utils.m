//
//  Utils.m
//  PaleoProject
//
//  Created by renan veloso silva on 22/02/14.
//  Copyright (c) 2014 renan veloso silva. All rights reserved.
//

#import "Utils.h"
#import <CommonCrypto/CommonCrypto.h>
#import <CoreImage/CoreImage.h>

@implementation Utils

//Pega o tamanho da tela na orientação atual
+(CGRect) screenBoundsOnCurrentOrientation{
    CGRect screenBounds = [UIScreen mainScreen].bounds ;
    return screenBounds;
}

+(CGRect)keyboardRectWithNotification:(NSNotification*)notification{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    CGRect screenSize = [self screenBoundsOnCurrentOrientation];
    CGFloat screenHeight = screenSize.size.height;
    
    keyboardFrameBeginRect.origin.y = screenHeight - keyboardFrameBeginRect.size.height;
    
    return keyboardFrameBeginRect;
}


+(id)loadNibForName:(NSString *)nibName{
    
    NSArray *list = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    if (list) {
        return  [list lastObject];
    }
    
    return nil;
}

@end

@implementation UIView (Additions)

//centraliza a view horizontalmente em relação a view pai
-(void)centerHorizontal{
    [self setX:([self.superview widthSize]/2)- ([self widthSize]/2)];
}

-(void)centerHorizontalWithSuperView:(UIView*)sv{
    [self setX:([sv widthSize]/2)- ([self widthSize]/2)];
}

-(void)centerWithSuperView:(UIView*)sv{
    [self setX:([sv widthSize]/2)- ([self widthSize]/2)];
    [self setY:([sv height]/2)- ([self height]/2)];
}

//centraliza a view verticalmente em relação a view pai
-(void)centerVertical{
    [self setY:([self.superview height]/2)- ([self height]/2)];
}

//centraliza a view em relação a view pai
-(void)centerInSuperview{
    [self centerHorizontal];
    [self centerVertical];
}

-(void)setX:(float)newX{
    CGRect frame = self.frame;
    frame.origin.x = newX;
    self.frame = frame;
}

-(float)x{
    return self.frame.origin.x;
}

-(void) setX_CG:(CGFloat) newX{
    [self setX:newX];
}

-(CGFloat)x_CG{
    return [self x];
}

-(void) setY_CG:(CGFloat) newY{
    [self setY:newY];
}

-(CGFloat)y_CG{
    return [self y];
}

-(void) setY:(float) newY{
    CGRect frame = self.frame;
    frame.origin.y = newY;
    self.frame = frame;
}
-(float)y{
    return self.frame.origin.y;
}

-(void) setWidth:(CGFloat) newWidth{
    CGRect frame = self.frame;
    frame.size.width = newWidth;
    self.frame = frame;
}
-(CGFloat)widthSize{
    return self.frame.size.width;
}

-(void) setHeight:(CGFloat) newHeight{
    CGRect frame = self.frame;
    frame.size.height = newHeight;
    self.frame = frame;
}
-(CGFloat)height{
    return self.frame.size.height;
}

-(void)removeSubviews{
    for (id obj in self.subviews) {
        [obj removeFromSuperview];
    }
}

-(void)setXYToZero{
    [self setX:0];
    [self setY:0];
}

-(void)clearColor{
    self.backgroundColor = [UIColor clearColor];
}

@end

@implementation NSString (JRAdditions)

+ (BOOL)isStringEmpty:(NSString *)string {
    if([string length] == 0) { //string is empty or nil
        return YES;
    }
    
    if(![[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
        //string is all whitespace
        return YES;
    }
    
    return NO;
}


@end

@implementation UIColor (Aditions)

+ (UIColor *) colorWithHexString: (NSString *) hexString {
    if([hexString isEqualToString:@""]){
        return [UIColor clearColor];
    }
    
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}


@end

@implementation NSDictionary (Aditions)


-(NSString*)safeStringForKey:(NSString*)string{
    id obj = [self objectForKey:string];
    if ([obj isKindOfClass:[NSString class]]) {
        NSString *str = (NSString*)obj;
        if (![NSString isStringEmpty:str]) {
            return str;
        }else{
            return @"";
        }
    }else{
        NSLog(@"key string invalid: %@", string);
    }
    return @"";
}

-(NSNumber*)safeNumberForKey:(NSString*)string{
    id obj = [self objectForKey:string];
    if ([obj isKindOfClass:[NSNumber class]]) {
        return (NSNumber*)obj;
    }else{
        NSLog(@"key number invalid: %@", string);
    }
    return @0;
}

@end


