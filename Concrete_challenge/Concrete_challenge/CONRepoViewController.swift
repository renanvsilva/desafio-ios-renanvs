//
//  ViewController.swift
//  Concrete_challenge
//
//  Created by renan silva on 9/18/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import UIKit

class CONRepoViewController: CONBaseViewController {

    var nextPage : Int = 1
    var listRepo = [CONRepoModel]()
    var lastModel : CONRepoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Github Java"
        lastModel = nil
        getRepos(page: nextPage)
    }
    
    func getRepos(page : Int){
        CONRequestService.requestRepos(page: page, repoResult: { (repos : [CONRepoModel]) in
            self.listRepo += repos
            self.tableView.reloadData()
            self.nextPage += 1
        }) { (e : Error?) in
                print("Something is wrong")
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CONRepoCell") as! CONRepoCell
        let model = listRepo[indexPath.row]
        cell.setupRepo(model: model)
        
        if indexPath.row == (listRepo.count - 1){
            getRepos(page: nextPage)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lastModel = listRepo[indexPath.row]
        self.performSegue(withIdentifier: "showPull", sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listRepo.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPull"{
            let pullController = segue.destination as! CONPullRequestsController
            pullController.repoModel = lastModel
        }
    }
}

