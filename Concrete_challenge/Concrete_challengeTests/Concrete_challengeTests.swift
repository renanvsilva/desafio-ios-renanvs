//
//  Concrete_challengeTests.swift
//  Concrete_challengeTests
//
//  Created by renan silva on 9/18/16.
//  Copyright © 2016 rvs. All rights reserved.
//

import XCTest
@testable import Concrete_challenge

class CONCRETEAppTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    //Test of Class CONPullRequestsController
    func testIfRepositoryNameAppearInNavigationBar() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let vc = CONPullRequestsController()
        vc.repoModel = CONRepoModel()
        vc.repoModel.repoName = "Teste"
        vc.repoModel.ownerName = "Renan"
        
        vc.viewDidLoad()
        
        XCTAssertEqual(vc.repoModel.repoName, vc.title)
    }
    
    //Test of Class CONPullModel
    func testIfDateFriendlyWorksWithGithubDataModel(){
        //Github date format: 2016-09-17T17:20:35Z
        let inValue = "2016-09-17T17:20:35Z"
        let outValue = "17/09/2016"
        
        let pullModel = CONPullModel()
        pullModel.date = inValue
        
        XCTAssertEqual(pullModel.dateFriendly, outValue)
    }
    
    //Test of Class CONRepoCell
    func testQtyFormatOfForkAndStarMust(){
        let repoModel1 = CONRepoModel()
        repoModel1.qtyFork = 10
        repoModel1.qtyStar = 1000
        
        let repoModel2 = CONRepoModel()
        repoModel2.qtyFork = 100000
        repoModel2.qtyStar = 1000000
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let controller = sb.instantiateViewController(withIdentifier: "CONRepoViewController") as! CONRepoViewController
        controller.viewDidLoad()
        controller.listRepo = [repoModel1, repoModel2]
        controller.tableView.reloadData()
        
        let repoCell1 = controller.tableView.visibleCells[0] as! CONRepoCell
        let repoCell2 = controller.tableView.visibleCells[1] as! CONRepoCell
        
        let expectResult1 = "10"
        let expectResult2 = "1k"
        
        let expectResult3 = "100k"
        let expectResult4 = "1M"
        
        let ok1 = repoCell1.qtyForkLabel.text == expectResult1
        let ok2 = repoCell1.qtyStarLabel.text == expectResult2
        let ok3 = repoCell2.qtyForkLabel.text == expectResult3
        let ok4 = repoCell2.qtyStarLabel.text == expectResult4
        let allOk = (ok1 && ok2 && ok3 && ok4) ? true : false
        XCTAssertEqual(allOk, true)
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
